import React from 'react'
import { DocsThemeConfig } from 'nextra-theme-docs'

const config: DocsThemeConfig = {
  logo: <span>TKB Studios</span>,
  project: {
    link: 'https://gitlab.com/TKB_Studios/tkbstudios-docs/',
  },
  chat: {
    link: 'https://discord.gg/yxjA6dnJ6s',
  },
  docsRepositoryBase: 'https://gitlab.com/TKB_Studios/tkbstudios-docs/-/tree/main/',
  footer: {
    text: 'TKB Studios Gitlab',
  },
}

export default config
